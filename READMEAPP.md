# Node code challenge
A geosearch engine express app.

# Getting Started
Run  npm i to install required packages and then npm start to run the app. Navigated to 'localhost:3000' to try out.

## Testing
Run npm test to run tests.