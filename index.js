// Server code goes here
const express = require('express');
var sqlite3 = require('sqlite3')
const app = express()
const { ErrorHandler } = require('./error.js');

var db = new sqlite3.Database('locations.db')

app.use(express.urlencoded({ extended: false }));

app.get('/', function(req, res) {
    res.sendFile('index.html', { root : 'public'});
  });

app.get('/locations', (req, res) => {
    const { q } = req.query;
    if (!q) {
      throw new ErrorHandler(400, 'Fuzzy match string is a required parameter')
    }
    if (q.length < 2){
      throw new ErrorHandler(400, 'Query string must have length >= 2')
    }
    db.all(`SELECT l.name, l.latitude, l.longitude FROM locations l WHERE l.name LIKE '${q}%'`, (err, allRows) => {
        if (err) {
          throw new ErrorHandler(500, 'An unknown error occured')
        }
        res.send(allRows.map(x => [x.name, x.latitude, x.longitude]).sort((a, b) => a[0].length - b[0].length))
      });
})

app.use((error, req, res, next) => {
  res.status(error.statusCode)
  res.json({
    status: 'error',
    message: error.message
  })
})

app.listen(3000, () => {
    console.log('Express app listening on port 3000')
})


module.exports = app