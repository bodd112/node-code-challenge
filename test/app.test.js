const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const app = require('../index.js');

chai.use(chaiHttp);

describe('/locations', () => {
    describe('query parameter not supplied', () => {
      it('should throw an error if query parameter not supplied', async () => {
        await chai.request(app)
          .get(`/locations`)
          .then((res) => {
            expect(res.status).to.equal(400)
            expect(res.body.status).to.equal('error')
            expect(res.body.message).to.equal('Fuzzy match string is a required parameter')
          })
      })
    })
    describe('query parameter zero length', () => {
      it('should throw an error if query parameter length is < 2', async () => {
        await chai.request(app)
          .get(`/locations?q=a`)
          .then((res) => {
            expect(res.status).to.equal(400)
            expect(res.body.status).to.equal('error')
            expect(res.body.message).to.equal('Query string must have length >= 2')
          })
      })
    })
    describe('matches found', () => {
      it('should return array of matches in alphabetical order', async () => {
        const testQuery = 'rab'
        await chai.request(app)
          .get(`/locations?q=${testQuery}`)
          .then(({ body }) => {
            expect(body).to.be.an('array');
            expect(body.length).to.equal(3);
            expect(body[0]).to.be.an('array');
            expect(body[0].length).to.equal(3);
            const compar = body.sort((a,b) => a[0].length - b[0].length)
            expect(body).to.deep.equal(compar);
          })
      })
    })
    describe('no matches found', () => {
        it('should return an empty array', async () => {
            const testQuery = '!?'
            await chai.request(app)
              .get(`/locations?q=${testQuery}`)
              .then(({ body }) => {
                expect(body).to.be.an('array');
                expect(body.length).to.equal(0);
              })
        })
      })
})