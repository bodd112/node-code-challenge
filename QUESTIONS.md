# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
The first call runs the set setTimeout function which will output '1' to the console after 100 ms. The following line is to print out '2' to the console which executes immediately and so the output becomes:
2
1


Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

foo is a recursive function which will continue to call itself whilst the argument it is called with is < 10. Until this condition is met the function calls itself with the input argument  incremented by 1. Once the input argument is equal to 10, the condition for recursion is no longer met and the function returns, executing each foo function in turn starting with the final value for the input argument and so the output is:
10
9
8
7
6
5
4
3
2
1
0


Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
The || operator will set the value of d to be 5 if the supplied initial value of d is falsy. In JS 0 is falsy and so if foo is provided with a value for d of 0, it will still set the value of d to be 5.

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

The function foo returns a nested function which is intialized with the supplied input argument which is set equal to the variable bar. Bar itself is therefore the function x => 1 + x
bar is then called with the argument 2 and so the output becomes 2 => 1 + 2 = 3.


Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

The function would be used to return double the supplied input argument after 100 ms.